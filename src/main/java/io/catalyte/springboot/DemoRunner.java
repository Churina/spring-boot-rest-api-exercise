package io.catalyte.springboot;

import io.catalyte.springboot.entities.Employee;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DemoRunner {
    public static void main(String[] args) {
        SpringApplication.run(DemoRunner.class);
    }
}
