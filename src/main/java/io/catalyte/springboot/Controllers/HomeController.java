package io.catalyte.springboot.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @RequestMapping(value = "/MissingPage",method = RequestMethod.GET)
    public String missingPage(){
        return "...Then I was found…";
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String Hello(){
        return "Hello World!";
    }

    @RequestMapping(value = "/secondPage",method = RequestMethod.GET)
    public String secondPage(){
        return "...Then I was found22222…";
    }

    @RequestMapping(value = "/cat",method = RequestMethod.GET)
    public String cat(){
        return "...meow…";
    }
}
