package io.catalyte.springboot.Controllers;


import io.catalyte.springboot.customexceptions.EmployeeNotFound;
import io.catalyte.springboot.entities.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/employees")
public class EmployeeController {
    private List<Employee> employees = new ArrayList<>();
    private int autoNumber = 0;
    // create employees
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Employee addEmployee(@RequestBody Employee employee){
        autoNumber++;
        employee.setEmployeeId(autoNumber);
        employees.add(employee);
        System.out.println(employees.size() + " employees.");
        return employee;
    }

      // initialize data
//    @RequestMapping(method = RequestMethod.POST, params = "initialize")
//    @ResponseStatus(HttpStatus.CREATED)
//    public Employee addEmployeeSamples(){
//        Employee newEmployee1 = new Employee(1,"Churina", "Sa", "20", true);
//        Employee newEmployee2 = new Employee(2,"John", "Doe", "30", false);
//        employees.add(newEmployee1);
//        employees.add(newEmployee2);
//        return null;
//    }



    // get all employees
    @RequestMapping(method = RequestMethod.GET)
    public List<Employee> getEmployees(){
        return employees;
    }


    //get employee by id
    @RequestMapping(value = "/{employeeId}", method = RequestMethod.GET)
    public Employee getEmployee(@PathVariable int employeeId){
        for (Employee e: employees) {
            if (e.getEmployeeId() != null && e.getEmployeeId() == employeeId) {
                return e;
            }
        }
        throw new EmployeeNotFound();
    }


    // get employee by firstName
    @RequestMapping(method = RequestMethod.GET, params = "firstName")
    public Employee getEmployeesWithActive(@RequestParam() String firstName){
        for(Employee e: employees){
            if (firstName.equals(e.getFirstName())){
                return e;
            }
        }
        throw new EmployeeNotFound();
    }


    // get employee by active status
    @RequestMapping(method = RequestMethod.GET, params = "isActive" )
    public List<Employee> getEmployeesByActiveStatus(Boolean isActive){
        System.out.println("it's active working");
        // create active and not active employee lists
        List<Employee> activeEmployees = new ArrayList<>();
        List<Employee> notActiveEmployees = new ArrayList<>();

        for (Employee e: employees){
            if (e.getActive()){
                // add employees who is active to activeEmployees
                activeEmployees.add(e);
                System.out.println("added employee e." + e);
            } else {
                // add employees who is not active to notActiveEmployees
                notActiveEmployees.add(e);
            }
        }

        if (isActive){
            return activeEmployees;
        } else {
            return notActiveEmployees;
        }
    }


    // update employee by id
    @RequestMapping(value = "/{employeeId}", method = RequestMethod.PUT)
    public Employee updateEmployeeById(@PathVariable int employeeId, @RequestBody Employee employee){
        for (Employee e: employees){
            if(e.getEmployeeId() == employeeId){
                int index = employees.indexOf(e);
                employees.set(index,employee);
                return employee;
            }
        }
        throw new EmployeeNotFound();
    }


    //delete employee by id
    @RequestMapping(value = "/{employeeId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable int employeeId){
        for (Employee e: employees){
            if (e.getEmployeeId() == employeeId){
                employees.remove(e);
                return;
            }
        }
        throw new EmployeeNotFound();
    }

}
